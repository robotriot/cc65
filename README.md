CC65 Docker image ready for compiling

Based on Ubuntu Linux LTS.

Contains latest:
- cca65 (https://github.com/cc65/cc65)

To compile your project, mount your project directory as `/src/` into the container and execute c*65 with any parameters you desire.
```
docker run --volume /your/project/dir:/src/ registry.gitlab.com/robotriot/cc65 ca65 your-source-file.s
```

E.g. to start assembler compilation of `main.s` from inside your project directory, use:
```
docker run --volume "$(pwd)":/src/ registry.gitlab.com/robotriot/cc65 ca65 main.s
```
