FROM ubuntu:20.04 AS cc65

# https://github.com/cc65/cc65/releases
ARG CC65="cc65-2.19.tar.gz"
ARG PREFIX="/usr/local/cc65"
ENV PREFIX="${PREFIX}"

RUN set -xe \
    && apt-get update \
    && apt-get install -y --no-install-recommends build-essential

WORKDIR /usr/src/cc65

COPY "${CC65}" .

RUN set -xe \
    && tar --strip-components=1 -xvzf "${CC65}" \
    && make \
    && make install


FROM ubuntu:20.04

ARG PREFIX="/usr/local/cc65"
ENV PREFIX="${PREFIX}"

RUN set -xe \
    && apt-get update \
    && apt-get install -y --no-install-recommends make \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && mkdir -p /src/

WORKDIR /src/

COPY --from=cc65 "${PREFIX}" "${PREFIX}"
ENV PATH="${PREFIX}/bin:${PATH}"
